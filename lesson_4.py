print("Задание 1__Сумма натуральных чисел")
a = int(input("Введите начальное значение ряда =>"))
b = int(input("Введите конечное значение ряда =>"))
a_1 = a
if b > a != b and a > 0:
    summ = a
    while a < b:
        a += 1
        summ += a
    print(f"Сумма чисел от {a_1} до {b} составляет {summ} ")
else:
    print("""Значения не могут быть меньше или равно 0!
Также начальное значение не может быть больше или равно конечному!""")
print('-' * 30)

print("Задание 2__Нахождения факториала числа")
n = int(input("Введите число для нахождения фаториала => "))
if n >= 0:
    factorial = 1
    for i in range(2, n + 1):
        factorial *= i
    print(factorial)
else:
    print("Вы ввели отрицательное число!")
print('-' * 30)

print("Задание 3__Вывод прямоугольного треугольника")
size = abs(int(input("Введите размерность (целое число) =>")))
for i in range(size + 1):
    for j in range(i):
        print('*', end="")
    print()
print('-' * 30)

print("Задание 4__Cума чисел, кратных среднему арифметическому этого промежутка. ")
a = int(input("Введите начальное значение ряда =>"))
b = int(input("Введите конечное значение ряда =>"))
a_1 = a
avg = 0
flag_natur_number = 2
if b > a != b and a > 0:
    avg = (a + b) / 2
    flag_natur_number = (a + b) % 2
    print(a, b, avg)

match flag_natur_number:
    case 0:
        summ_k = 0
        while a <= b:
            if (a % avg) == 0:
                summ_k += a
            a += 1
        print(f"Сумма чисел, кратное средне арифметическому в промежутке {a_1} ..{b} составляет: {summ_k}")
    case 1:
        print("Среднее арифметическое не натуральное число. Суммы чисел нет.")
    case 2:
        print("""Значения не могут быть меньше или равно 0!
Также начальное значение не может быть больше или равно конечному!""")

print('-' * 30)

print("Задание 5__Вывод прямоугольника")
rows = int(input("Сколько строк =>"))
cols = int(input("Сколько столбцов =>"))

for r in range(rows):
    for c in range(cols):
        print('*', end='')
    print()
print('-' * 30)

print("Задание 6__Программа авторизации")
LOGIN = "abc"
PASSWORD = "123"
i = 1
while i < 4:
    login_1 = input("Введите логин =>").strip()
    password_1 = input("Введите пароль =>").strip()
    if login_1 == LOGIN and password_1 == PASSWORD:
        print(f"«Авторизация успешно пройдена с «{i}» попыт(ки/ок)»")
        break
    else:
        i += 1
else:
    print(f"«Авторизация не пройдена. Вы использовали «{i - 1}» попыт(ки/ок)»")

print('-' * 30)
