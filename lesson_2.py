print("Задание 1 __Вывод двух слов, разделённых запятыми")
word_1 = input("Введите первое слово =>")
word_2 = input("Введите второе слово =>")
print(word_1, word_2, sep=',')#print(word_1 +"," + word_2)
print("----------")

print("Задание 2 __Произведение введённых трех чисел")
a = int(input("Введите число a =>"))
b = int(input("Введите число b =>"))
x = int(input("Введите число x =>"))
print(f"Произведения введенных трех чисел a,b и x равно = {a * b * x}")
print("----------")

print("Задание 3 __Программирование формулы")
a = int(input("Введите a (любое целое число кроме '0') =>"))
b = int(input("Введите b =>"))
c = int(input("Введите с =>"))
x_1 = (-b + (pow((b ** 2 - 4 * a * c), 0.5))) / 2 * a
x_2 = (-b - (pow((b ** 2 - 4 * a * c), 0.5))) / 2 * a
print("Результат: ", f"x_1= {x_1} x_2= {x_2}")
print("----------")

print("Задание 4 __Cумма ASCII-кодов введённой фразы")
fraza = input("Введите фразу из 10 символов =>")
if len(fraza) == 10:
    sum_ascii_fraza = 0
    for ch in fraza:
        sum_ascii_fraza += ord(ch)
    print(f"Cумма ASCII-кодов символов введёной строки равна => {sum_ascii_fraza}")
else:
    print(f"Сумма не подсчитанна, так как вы ввели {len(fraza)} символов")
print("----------")

print("Задание 5 __Строка в обратном порядке")
stroka = input("Введите строку =>")
print("Строка в обратном порядке: ", stroka[::-1])
print("----------")

print("Задание 6 __Вычисление площади круга ")
import math

r = int(input("Введите радиус круга (целое число) =>"))
s = math.pi * pow(r, 2)
print("Площадь круга равняеться =>", s)
print("----------")

print("Задание 7 __Вычисление времени движения")
length = 700
velocity = 90
driving_time = length / velocity
print("Время движения: ", driving_time)
print("----------")

print("Задание 8 __Вывод заданного имени и возраста")
name = input("Введите имя =>")
age = input("Введите возраст =>")
print("My name is " + name + " and I am " + age)
# другой вариант решения без ввода с клавиатуры и использование преобразовния типa srt()
# name = "Max"
# age = 25
# print("My name is " + name +  " and I am " + str(age))
##print(f"My name is {name} and I am {str(age)}")
